package main;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class rutThamHashset {
	Set<String> thungPhieu = new HashSet<String>();
	public rutThamHashset() {
	}
	
	public boolean themPhieu(String giaTri) {
		return thungPhieu.add(giaTri);
	}
	
	public boolean xoaPhieu(String giaTri) {
		return thungPhieu.remove(giaTri);
	}
	
	public boolean kiemTraTonTai(String giaTri) {
		return thungPhieu.contains(giaTri);
	}
	
	public void xoaAllPhieu() {
		thungPhieu.clear();
	}
	
	public int soLuongPhieu() {
		return thungPhieu.size();
	}
	
	public String rutMotTham() {
		String ketQua = "";
		Random rd = new Random();
		int viTri = rd.nextInt(thungPhieu.size());
		ketQua = (String) thungPhieu.toArray()[viTri];
		return ketQua;
	}
	
	public void inTatCa() {
//		System.out.print("[ ");
//		for (String element : thungPhieu) {
//            System.out.print(element);
//            System.out.print("  ");
//        }
//		System.out.print("]");
		System.out.println(Arrays.toString(thungPhieu.toArray()));
	}
}
