package main;

import java.util.Scanner;

public class test {
	public static void main(String[] args) {
		rutThamHashset rt = new rutThamHashset();
//		rutThamTreeset rt2 = new rutThamTreeset();
		int luaChon = 0;
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("-------------------------MENU-------------------------");
			System.out.println("1. Thêm mã số dự thưởng.");
			System.out.println("2. Xóa mã số dự thưởng.");
			System.out.println("3. Kiểm tra mã số dự thưởng có tồn tại hay không?");
			System.out.println("4. Xóa tất cả các phiếu dự thưởng.");
			System.out.println("5. Số lượng phiếu dự thưởng.");
			System.out.println("6. Rút thăm trúng thưởng.");
			System.out.println("7. In ra tất cả các phiếu.");
			System.out.println("0. Thoát khỏi chương trình");
			System.out.println("Nhập lựa chọn: "); luaChon = sc.nextInt();
			sc.nextLine();
			if(luaChon==1 || luaChon==2||luaChon==3) {
				System.out.println("Nhập vào mã phiếu dự thưởng: ");
				String giaTri = sc.nextLine();
				if(luaChon == 1) {
					rt.themPhieu(giaTri);
				}else if(luaChon==2) {
					rt.xoaPhieu(giaTri);
				}else {
					System.out.println("Kết quả kiểm tra: "+rt.kiemTraTonTai(giaTri));
				}
			} else if(luaChon == 4){
				rt.xoaAllPhieu();
			} else if(luaChon == 5){
				System.out.println("Số lượng phiếu là: " +rt.soLuongPhieu());
			} else if(luaChon == 6){
				System.out.println("Thăm trúng thưởng là: " +rt.rutMotTham());
			} else {
				System.out.println("Các mã phiếu là: ");
				rt.inTatCa();
//				System.out.println("\n");
			}
		} while (luaChon !=0);
	}
}
